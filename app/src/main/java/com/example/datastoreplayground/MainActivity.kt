package com.example.datastoreplayground

import android.content.Context
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.doOnTextChanged
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.intPreferencesKey
import androidx.datastore.preferences.core.stringPreferencesKey
import androidx.datastore.preferences.preferencesDataStore
import com.example.datastoreplayground.databinding.ActivityMainBinding
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch

var num = 0

class MainActivity : AppCompatActivity() {
    private val strKey = stringPreferencesKey("Str")

    /** Edit text -> Flow -> Prefs -> Listener -> TextView */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val editFlow = MutableSharedFlow<String>(extraBufferCapacity = 20)
        val scope = CoroutineScope(Dispatchers.Main)
        scope.launch {
            editFlow.collect{ newStr ->
                dataStore.edit { it[strKey] = newStr }
            }
        }

        binding.editTextBox.doOnTextChanged { text, _, _, _ ->
            editFlow.tryEmit(text.toString())
        }

        scope.launch {
            dataStore.edit { it[intPreferencesKey("kuku")] = ++num }
            Log.v("Tag", "Finished sending")
        }
        scope.launch {
            dataStore.data.collect {
                Log.v("Tag", "Is now $it")
                binding.textView.text = it[strKey] ?: ""
            }
            Log.v("Tag", "Finished receiving")
        }
    }

    private val Context.dataStore by preferencesDataStore(name = "Boo")
}